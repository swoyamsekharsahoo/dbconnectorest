package com.testscriptedsql;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.api.RemoteFrameworkConnectionInfo;
import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.Name;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.Uid;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDbApplication.class, args);
		
		ConnectorInfoManagerFactory fact = ConnectorInfoManagerFactory.getInstance();
		String password = "swoyamss";
		
		RemoteFrameworkConnectionInfo remoteFrameworkConnectionInfo = new RemoteFrameworkConnectionInfo("localhost", 8759,
				new GuardedString(password.toCharArray()));
        ConnectorInfoManager manager = fact.getRemoteManager(remoteFrameworkConnectionInfo);
        ConnectorKey key = new ConnectorKey("net.tirasa.connid.bundles.db.scriptedsql", "2.2.6", "net.tirasa.connid.bundles.db.scriptedsql.ScriptedSQLConnector");       
        ConnectorInfo info = manager.findConnectorInfo(key);
        APIConfiguration apiConfig = info.createDefaultAPIConfiguration();
        ConfigurationProperties properties = apiConfig.getConfigurationProperties();
        properties.setPropertyValue("host", "localhost");
        properties.setPropertyValue("port", "3306");
        properties.setPropertyValue("user", "root");
        properties.setPropertyValue("password", new GuardedString("ssss.9498".toCharArray()));
        properties.setPropertyValue("database", "admin");
        properties.setPropertyValue("jdbcDriver", "com.mysql.cj.jdbc.Driver");
        properties.setPropertyValue("jdbcUrlTemplate", "jdbc:mysql://localhost:3306/testscriptedsql");
        properties.setPropertyValue("testScriptFileName", "D:\\internship\\TestDB\\src\\main\\resources\\TestScript.groovy");
        properties.setPropertyValue("createScriptFileName", "D:\\internship\\TestDB\\src\\main\\resources\\CreateScript.groovy");
        properties.setPropertyValue("updateScriptFileName", "D:\\internship\\TestDB\\src\\main\\resources\\UpdateScript.groovy");
        properties.setPropertyValue("deleteScriptFileName", "D:\\internship\\TestDB\\src\\main\\resources\\DeleteScript.groovy");
        
//        properties.setPropertyValue("baseAddress", "https://dev112873.service-now.com");
        Set<Attribute> set = new HashSet<>();
        Map<String, Object> map = new HashMap<>();
//        map.put(Name.NAME, "5");
        map.put("firstname", "abhisek");
        map.put("lastname", "mohanty");
        map.put("fullname", "abhisek mohanty");
        map.put("email", "abhisek@gmail.com");
        map.put("organization", "nasa");
        
        for(Entry<String, Object> obj : map.entrySet())
        {
        	set.add(AttributeBuilder.build(obj.getKey(), obj.getValue()));
        }
        
        ConnectorFacade conn = ConnectorFacadeFactory.getInstance().newInstance(apiConfig);
        conn.validate();
        conn.test();
//        conn.create(ObjectClass.ACCOUNT, set, null);
        conn.update(ObjectClass.ACCOUNT, new Uid("5"), set, null);
//        conn.delete(ObjectClass.ACCOUNT, new Uid("4"), null);
	
	}

}
